let number = Number(prompt("Give me a number"));
	console.log("The number you provided is " + number + ".");

for(let count = 1; count <= 20; count++){

	if(number <= 50){
		console.log("The current number is at 50. Terminating the loop");
		break;
	}

	if(number % 10 === 0){
		console.log("This number is divisible by 10. Skipping the number.");
	}

	else{
		console.log(number);
	}

		number -= 5;

	{
		continue;
	}	
}

let string = "supercalifragilisticexpialidocious";
let newString = "";
console.log(string);

for(let i = 0; i < string.length; i++){

	if(
		string[i] == "a" ||
		string[i] == "e" ||
		string[i] == "i" ||
		string[i] == "o" ||
		string[i] == "u" 
	){
		continue;
	}
	else{
		newString = newString + string[i];
	}
}
console.log(newString);